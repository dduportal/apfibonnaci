package fr.ensg.sgruel.maven.core.apfibonnaci;

import org.junit.Test;
import org.junit.Assert;

class TestFibonnaci {
  public void testGetValueReturnZero(){
	
    Assert.assertEquals(0L,Fibonnaci.getValue(0L),0.0);
    Assert.assertEquals(0L,Fibonnaci.getValue(-1L),0.0);
    Assert.assertEquals(0L,Fibonnaci.getValue(Long.MIN_VALUE),0.0);
  }
  public void testGetValueReturnOne(){
    Assert.assertEquals(1L,Fibonnaci.getValue(1L),0.0);

  }
  public void testGetValueReturn(){
    Assert.assertEquals(233L,Fibonnaci.getValue(13L),0.0);
  }


}
