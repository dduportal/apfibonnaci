package fr.ensg.simeon.maven.springboot.apfibonnaci;


import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import fr.ensg.sgruel.maven.core.apfibonnaci.Fibonnaci;

/**
* API that compute fibonnaci sequence
*/
@RestController
public class Controller{
  @GetMapping("/fibonnaci/{rank}")
  public String fibonnaci(@PathVariable Long rank){
    return String.valueOf(Fibonnaci.getValue(rank));
  }
}
